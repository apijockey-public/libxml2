# Xml for Swift
 
The Xml for Swift package is a wrapper around the C-based library libxml2. It allows accessing libxml2 functionality from within swift (Unsafe Swift) and includes the libxml2 libraries a static dependency to your app.

## Build information

### Configuration in build process
Xml for Swift was compiled with the following configuration flags:
- --with-minimum 
- --with-c14n 
- --with-iconv 
- --with-output
- --with-reader
- --with-regexps
- --with-schemas
- --with-schematron
- --with-threads
-  --with-tree
- --with-valid
- --with-writer
- --with-xpath
- --with-xptr
- --with-xinclude
- --with-sax1

### Distribution format
The Package downloads the libxml2 binaries compiled for MacOS as binary in the XCFramework format.
OpenSSL.xcframework is distributed as a multiplatform XCFramework bundle, for more information checkout the documentation [Distributing Binary Frameworks as Swift Packages](https://developer.apple.com/documentation/xcode/distributing-binary-frameworks-as-swift-packages) 

### Adoption
Include the following dependency in your Package.swift (libmxl2 required libiconf), that's why you would include both dependencies here.

```swift
dependencies: [
    .package(url: "https://gitlab.com/apijockey-public/libxml2.git", from: "0.0.1"),
     .package(url: "https://gitlab.com/apijockey-public/libiconf.git", from: "0.0.3")
]
```

```swift
.target(
    name: "MyApp",
    dependencies: [
        .product(name: "Iconf", package: "libiconf"),
         .product(name: "Xml", package: "libxml2")
    ]
),
```
