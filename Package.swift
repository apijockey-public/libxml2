// swift-tools-version: 5.10
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Xml",
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "Xml",
            targets: ["Xml"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/apijockey-public/libiconf.git", from: "0.0.3"),
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .binaryTarget(
            name: "Xml",
            url: "https://apijockey.com/releases/downloads/libxml2/libxml2-2.9.10.xcframework.zip",
            checksum: "75da2297f5721ddf88bd05c2d0ffbb540483118df5bc542b490fdf4eec0fd2d4"
        )
        
    ]
    
)
